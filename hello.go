package main

import (
	"fmt"
	"github.com/beevik/ntp"
	"log"
	"os"
	"time"
)

func main() {
	HelloNow()
}

func HelloNow() time.Time {
	now, err := ntp.Time("pool.ntp.org")
	if err != nil {
		log.Println(err)
		os.Exit(1)
	}
	fmt.Println(now.Format(time.UnixDate))

	return now
}
